package com.yenhoang.gpxapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;

import com.yenhoang.gpxapi.property.FileStorageProperties;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@SpringBootApplication
@ComponentScan(basePackages = "com.yenhoang.gpxapi")
@EnableConfigurationProperties({
    FileStorageProperties.class
})
public class GpxapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(GpxapiApplication.class, args);
	}

}

