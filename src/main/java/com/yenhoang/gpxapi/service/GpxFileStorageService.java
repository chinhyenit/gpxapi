package com.yenhoang.gpxapi.service;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hs.gpxparser.GPXParser;
import com.hs.gpxparser.modal.GPX;
import com.hs.gpxparser.modal.Metadata;
import com.yenhoang.gpxapi.entity.Link;
import com.yenhoang.gpxapi.entity.MetaData;
import com.yenhoang.gpxapi.entity.WayPoint;
import com.yenhoang.gpxapi.property.FileStorageProperties;

@Service
public class GpxFileStorageService {
	private final Path fileStorageLocation;

	@Autowired
	public GpxFileStorageService(FileStorageProperties fileStorageProperties) {
		this.fileStorageLocation = Paths.get(fileStorageProperties.getUploadDir()).toAbsolutePath().normalize();
	}
	
	@Autowired
	private MetaDataService metaDataService;
	
	@Autowired
	private WayPointService wayPointService;
	
	public void store2Db(String fileName) {
		GPXParser p = new GPXParser();
//		FileInputStream in;
		try (FileInputStream in = new FileInputStream(Paths.get(this.fileStorageLocation.toString(), fileName).toFile())) {
//			in = new FileInputStream(Paths.get(this.fileStorageLocation.toString(), fileName).toFile());
			try {
				GPX gpx = p.parseGPX(in);
				if (gpx != null) {
					Metadata meta = gpx.getMetadata();
					MetaData metaData = new MetaData();
					metaData.setName(meta.getName());
					metaData.setDesc(meta.getDesc());
					metaData.setTime(meta.getTime());
					Set<Link> links = new HashSet<Link>();
					meta.getLinks().forEach(l -> {
						Link link = new Link();
						link.setHref(l.getHref());
						link.setText(l.getText());
						link.setMetaData(metaData);
						links.add(link);
					});
					metaData.setLinks(links);
					this.metaDataService.saveMetaData(metaData);
					// save waypoint
					if (gpx.getWaypoints() != null && !gpx.getWaypoints().isEmpty()) {
						gpx.getWaypoints().forEach(w -> {
							WayPoint waypoint = new WayPoint();
							waypoint.setLat(w.getLatitude());
							waypoint.setLon(w.getLongitude());
							waypoint.setName(w.getName());
							waypoint.setSym(w.getSym());
							this.wayPointService.saveWayPoint(waypoint);
						});
					}
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		

	}
}
