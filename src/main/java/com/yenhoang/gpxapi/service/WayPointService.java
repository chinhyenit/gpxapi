package com.yenhoang.gpxapi.service;

import com.yenhoang.gpxapi.entity.WayPoint;

public interface WayPointService {
	public void saveWayPoint(WayPoint waypoint);
}
