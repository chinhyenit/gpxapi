package com.yenhoang.gpxapi.service;

import com.yenhoang.gpxapi.entity.MetaData;

public interface MetaDataService {

	public void saveMetaData(MetaData metaData);
}
