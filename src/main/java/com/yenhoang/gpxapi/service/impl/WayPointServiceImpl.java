package com.yenhoang.gpxapi.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yenhoang.gpxapi.entity.WayPoint;
import com.yenhoang.gpxapi.repository.WayPointRepository;
import com.yenhoang.gpxapi.service.WayPointService;

@Service
public class WayPointServiceImpl implements WayPointService {
	
	@Autowired
	private WayPointRepository wayPointRepository;

	@Override
	public void saveWayPoint(WayPoint waypoint) {
		this.wayPointRepository.save(waypoint);
		
	}

}
