package com.yenhoang.gpxapi.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yenhoang.gpxapi.entity.MetaData;
import com.yenhoang.gpxapi.repository.MetaDataRepository;
import com.yenhoang.gpxapi.service.MetaDataService;

@Service
public class MetaDataServiceImpl implements MetaDataService {
	
	@Autowired
	private MetaDataRepository metaDataRepository;

	@Override
	public void saveMetaData(MetaData metaData) {
		metaDataRepository.save(metaData);
	}

}
