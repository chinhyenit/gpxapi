package com.yenhoang.gpxapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.yenhoang.gpxapi.entity.MetaData;

@Repository
public interface MetaDataRepository extends JpaRepository<MetaData, Long> {

}
