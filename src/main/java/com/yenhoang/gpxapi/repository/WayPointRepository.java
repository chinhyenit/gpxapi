package com.yenhoang.gpxapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.yenhoang.gpxapi.entity.WayPoint;

@Repository
public interface WayPointRepository extends JpaRepository<WayPoint, Long> {

}
